package com.moyq5.wifi.joystick.command.impl;

import com.moyq5.wifi.joystick.command.CommandAbstract;

/**
 * 通用控制指令
 */
public class JoystickCommand extends CommandAbstract {
    private boolean leftBt = false;
    private boolean rightBt = false;
    private boolean upBt = false;
    private boolean downBt = false;
    private boolean aBt = false;
    private boolean bBt = false;
    private boolean cBt = false;
    private boolean dBt = false;

    private static int serialNo = 0;

    @Override
    protected int getAckType() {
        return ACK_TYPE_REQ;
    }

    @Override
    protected int getCmdType() {
        return CMD_TYPE_ENJOY;
    }

    @Override
    protected int getSerialNo() {
        return serialNo;
    }

    @Override
    protected byte[] content() {
        byte[] content = new byte[1];
        content[0] |= leftBt ? 0x80: 0x00;  // 1000 0000
        content[0] |= rightBt ? 0x40: 0x00; // 0100 0000
        content[0] |= upBt ? 0x20: 0x00;    // 0010 0000
        content[0] |= downBt ? 0x10: 0x00;  // 0001 0000
        content[0] |= aBt ? 0x08: 0x00;     // 0000 1000
        content[0] |= bBt ? 0x04: 0x00;     // 0000 0100
        content[0] |= cBt ? 0x02: 0x00;     // 0000 0010
        content[0] |= dBt ? 0x01: 0x00;     // 0000 0001
        return content;
    }

    @Override
    public String toString() {
        return "DevCtrlCommand{" +
                "leftBt=" + leftBt +
                ", rightBt=" + rightBt +
                ", upBt=" + upBt +
                ", downBt=" + downBt +
                ", aBt=" + aBt +
                ", bBt=" + bBt +
                ", cBt=" + cBt +
                ", dBt=" + dBt +
                '}';
    }

    public void reset() {
        leftBt = false;
        rightBt = false;
        upBt = false;
        downBt = false;
        aBt = false;
        bBt = false;
        cBt = false;
        dBt = false;
    }

    public static int raiseSerialNo() {
        if (serialNo == 0xFFFF) {
            serialNo = 0;
        }
        return serialNo++;
    }

    public String format() {
        return "ackType=" + getAckType() + ",cmdType=" + getCmdType() + ",serialNo=" + getSerialNo()
                + (leftBt ? ",left":"") + (rightBt ? ",right":"") + (upBt ? ",up":"") +
                (downBt ? ",down":"") + (aBt ? ",A":"") + (bBt ? ",B":"") + (cBt ? ",C":"") + (dBt ? ",D":"");
    }

    public boolean isLeftBt() {
        return leftBt;
    }

    public void setLeftBt(boolean leftBt) {
        this.leftBt = leftBt;
    }

    public boolean isRightBt() {
        return rightBt;
    }

    public void setRightBt(boolean rightBt) {
        this.rightBt = rightBt;
    }

    public boolean isUpBt() {
        return upBt;
    }

    public void setUpBt(boolean upBt) {
        this.upBt = upBt;
    }

    public boolean isDownBt() {
        return downBt;
    }

    public void setDownBt(boolean downBt) {
        this.downBt = downBt;
    }

    public boolean isaBt() {
        return aBt;
    }

    public void setaBt(boolean aBt) {
        this.aBt = aBt;
    }

    public boolean isbBt() {
        return bBt;
    }

    public void setbBt(boolean bBt) {
        this.bBt = bBt;
    }

    public boolean iscBt() {
        return cBt;
    }

    public void setcBt(boolean cBt) {
        this.cBt = cBt;
    }

    public boolean isdBt() {
        return dBt;
    }

    public void setdBt(boolean dBt) {
        this.dBt = dBt;
    }

}
