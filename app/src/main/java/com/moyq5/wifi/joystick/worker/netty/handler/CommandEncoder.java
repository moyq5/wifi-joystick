package com.moyq5.wifi.joystick.worker.netty.handler;

import com.moyq5.wifi.joystick.command.Command;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class CommandEncoder extends MessageToByteEncoder<Command> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Command msg, ByteBuf out) throws Exception {
        byte[] data = msg.bytes();
        out.writeBytes(data);
    }
}
