package com.moyq5.wifi.joystick.worker;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;


public abstract class WorkerFacory {
    private static final Constraints constraints = new Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build();

    public static void beginWorkers(Context context) {
        //enqueveNettyServerOneTime(context);
    }

    public static void cancelWorkers(Context context) {
        WorkManager.getInstance(context).cancelAllWork();
    }

    public static void enqueveNettyServerOneTime(Context context, String host) {
        Data data = new Data.Builder()
                .putString("host", host)
                .build();

        OneTimeWorkRequest.Builder builder = new OneTimeWorkRequest
                .Builder(NettyServerWorker.class)
                .setInputData(data);

        WorkManager.getInstance(context)
                .enqueueUniqueWork(NettyServerWorker.class.getSimpleName(),
                        ExistingWorkPolicy.KEEP,
                        builder.build());
    }


    public static void enqueveNettyClientOneTime(Context context, String host, int port) {
        Data data = new Data.Builder()
                .putString("host", host)
                .putInt("port", port)
                .build();

        OneTimeWorkRequest.Builder builder = new OneTimeWorkRequest
                .Builder(NettyClientWorker.class)
                .setInputData(data);

        WorkManager.getInstance(context).enqueue(builder.build());
    }

}
