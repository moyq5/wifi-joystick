package com.moyq5.wifi.joystick.command;

/**
 * p2p设备通讯指令抽象
 * @author mo_yq5
 * @since 2021/11/25
 */
public abstract class P2pCommandAbstract extends CommandAbstract {
    /**
     * 透传类型
     */
    private P2pPassType passType = P2pPassType.NOT;
    /**
     * 透传线路，如设备有多路SPI接口，通过此值指明使用哪路SPI，从0开始
     */
    private int passRoute = 0;

    @Override
    protected final byte[] content() {
        byte[] pass = passContent();
        byte[] content = new byte[1 + pass.length];
        // 透传类型占后4位
        content[0] |= (passType.ordinal() & 0x0F) << 4;
        // 透传线路占前4位
        content[0] |= (passRoute & 0x0F);
        System.arraycopy(pass, 0, content, 1, pass.length);
        return content;
    }

    @Override
    protected int getCmdType() {
        return CMD_TYPE_P2P;
    }

    protected abstract byte[] passContent();

    public P2pPassType getPassType() {
        return passType;
    }

    public void setPassType(P2pPassType passType) {
        this.passType = passType;
    }

    public int getPassRoute() {
        return passRoute;
    }

    public void setPassRoute(int passRoute) {
        this.passRoute = passRoute;
    }

}
