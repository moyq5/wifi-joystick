package com.moyq5.wifi.joystick.command.impl;

import com.moyq5.wifi.joystick.command.CommandAbstract;

import java.nio.charset.StandardCharsets;

/**
 * wifi账号指令，即控制p2p设备连接wifi
 */
public class WifiConnectCommand extends CommandAbstract {
    /**
     * wifi账号SSID
     */
    private String ssid;
    /**
     * wifi账号密码
     */
    private String password;

    private static int serialNo = 0;

    @Override
    protected int getAckType() {
        return ACK_TYPE_REQ;
    }

    @Override
    protected int getCmdType() {
        return CMD_TYPE_WIFI;
    }

    @Override
    protected int getSerialNo() {
        return serialNo;
    }

    @Override
    protected byte[] content() {
        byte[] ssidBytes = ssid.getBytes(StandardCharsets.UTF_8);
        byte[] pwdBytes = password.getBytes(StandardCharsets.UTF_8);
        byte[] content = new byte[1 + ssidBytes.length + pwdBytes.length];
        // 第1字节指出SSID长度，剩下的为密码内容
        content[0] = (byte) (ssidBytes.length & 0xFF);
        System.arraycopy(ssidBytes, 0, content, 1, ssidBytes.length);
        System.arraycopy(pwdBytes, 0, content, 1 + ssidBytes.length, pwdBytes.length);
        return content;
    }

    @Override
    public String format() {
        return "ackType=" + getAckType() + ",cmdType=" + getCmdType() + ",serialNo=" + getSerialNo()
                + ",ssid=" + ssid + ",pwd=" + password;
    }

    public static int raiseSerialNo() {
        if (serialNo == 0xFFFF) {
            serialNo = 0;
        }
        return serialNo++;
    }


    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
