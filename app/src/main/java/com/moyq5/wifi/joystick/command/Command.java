package com.moyq5.wifi.joystick.command;

/**
 * 指令
 * @author mo_yq5
 * @since 2021/11/25
 */
public interface Command {
    /**
     * 请求
     */
    int ACK_TYPE_REQ = 0;
    /**
     * 响应
     */
    int ACK_TYPE_RES = 1;
    /**
     * WIFI点对点指令
     */
    int CMD_TYPE_P2P = 0;
    /**
     * 连接WIFI
     */
    int CMD_TYPE_WIFI = 1;
    /**
     * 游戏控制
     */
    int CMD_TYPE_ENJOY = 2;

    byte[] bytes();
}
