package com.moyq5.wifi.joystick;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.moyq5.wifi.joystick.worker.WorkerFacory;

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        WorkerFacory.beginWorkers(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        WorkerFacory.cancelWorkers(this);
    }

    public static Context getInstance() {
        return context;
    }

    public static void localSend(Context context, Intent intent) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void localRegister(Context context, BroadcastReceiver receiver, IntentFilter filter) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
    }

    public static void localUnregister(Context context, BroadcastReceiver receiver) {
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
        } catch (Exception ignored) {

        }

    }

}
