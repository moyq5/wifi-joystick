package com.moyq5.wifi.joystick;

import android.annotation.SuppressLint;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class ThreadKit {

    private static final ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(10);
    private static final ExecutorService cachedService = Executors.newCachedThreadPool();

    private ThreadKit() {

    }

    public static Future<?> submit(Runnable task) {
        return cachedService.submit(task);
    }

    public static Future<?> submit(Runnable task, LifecycleOwner owner) {
        return registerLifecycle(owner, cachedService.submit(task));
    }

    public static void execute(Runnable task) {
        cachedService.execute(task);
    }

    public static ExecutorService pool() {
        return cachedService;
    }

    public static ScheduledFuture<?> schedule(Runnable task, long delay, TimeUnit unit) {
        return scheduledService.schedule(task, delay, unit);
    }

    public static ScheduledFuture<?> schedule(Runnable task, long delay, TimeUnit unit, LifecycleOwner owner) {
        return (ScheduledFuture<?>)registerLifecycle(owner, scheduledService.schedule(task, delay, unit));
    }

    public static ScheduledFuture<?> schedule(Runnable task, long initialDelay, long period, TimeUnit unit) {
        return scheduledService.scheduleAtFixedRate(task, initialDelay, period, unit);
    }

    public static ScheduledFuture<?> schedule(Runnable task, long initialDelay, long period, TimeUnit unit, LifecycleOwner owner) {
        return (ScheduledFuture<?>)registerLifecycle(owner, scheduledService.scheduleAtFixedRate(task, initialDelay, period, unit));
    }

    public static void release() {
        scheduledService.shutdownNow();
        cachedService.shutdownNow();
    }

    private static Future<?> registerLifecycle(LifecycleOwner owner, Future<?> future) {
        owner.getLifecycle().addObserver(new LifecycleObserver() {

            @SuppressLint("RestrictedApi")
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            public void onStop(LifecycleOwner owner) {
                future.cancel(true);
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            public void onDestroy(LifecycleOwner owner) {
                future.cancel(true);
                owner.getLifecycle().removeObserver(this);
            }
        });
        return future;
    }
}
