package com.moyq5.wifi.joystick.worker.netty.handler;

import com.moyq5.wifi.joystick.Console;
import com.moyq5.wifi.joystick.ConsoleFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class DiscardServerHandler extends ChannelInboundHandlerAdapter {
    private static final Console console = ConsoleFactory.get(DiscardServerHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        console.info("设备上线：%s", ctx.channel().remoteAddress().toString());
        ByteBuf buf = ctx.alloc().buffer(4).writeBytes("123".getBytes());
        ctx.writeAndFlush(buf);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        console.info("设备离线：%s", ctx.channel().remoteAddress().toString());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) { // (2)
        // Discard the received data silently.
        console.info("收到数据");
        ((ByteBuf) msg).release(); // (3)
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        super.userEventTriggered(ctx, evt);
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
        super.channelWritabilityChanged(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        console.error(cause, "数据异常");
        cause.printStackTrace();
        ctx.close();
    }
}
