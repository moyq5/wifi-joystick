package com.moyq5.wifi.joystick;

/**
 * 日志控制台工厂类
 * @author mo_yq5
 * @since 2021-11-23
 */
public abstract class ConsoleFactory {

    public static Console get(Class<?> clazz) {
        return new ConsoleDefault(clazz);
    }
}
