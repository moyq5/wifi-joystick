package com.moyq5.wifi.joystick;

public interface Console {
    String PARAM_LEVEL = "level";
    String PARAM_MESSAGE = "message";
    int DEBUG = 1;
    int INFO = 2;
    int WARN = 3;
    int ERROR = 4;

    void debug(String format, Object... args);

    void info(String format, Object... args);

    void warn(String format, Object... args);

    void error(String format, Object... args);

    void error(Throwable cause, String format, Object... agrs);

}
