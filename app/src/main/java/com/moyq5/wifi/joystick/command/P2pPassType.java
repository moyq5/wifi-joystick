package com.moyq5.wifi.joystick.command;

/**
 * p2p 指令透传类型
 * @author mo_yq5
 * @since 2021/11/25
 */
public enum P2pPassType {
    /**
     * P2P设备内部消费，不进行透传
     */
    NOT,
    /**
     * 通过SPI接口透传
     */
    SPI,
    /**
     * 通过UART接口透传
     */
    UART,
    /**
     * 通过I2C接口透传
     */
    I2C;
}
