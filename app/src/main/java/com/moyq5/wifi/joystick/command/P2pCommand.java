package com.moyq5.wifi.joystick.command;

/**
 * p2p指令
 * @author mo_yq5
 * @since 2021/11/26
 */
public class P2pCommand extends P2pCommandAbstract {

    private static int serialNo = 0;

    private CommandAbstract passCommand;

    @Override
    protected int getAckType() {
        return ACK_TYPE_REQ;
    }

    @Override
    protected int getSerialNo() {
        return serialNo;
    }

    @Override
    protected final byte[] passContent() {
        if (null != passCommand) {
            return passCommand.bytes();
        }
        return new byte[0];
    }

    public void setPassCommand(CommandAbstract passCommand) {
        this.passCommand = passCommand;
    }

    public static int raiseSerialNo() {
        if (serialNo == 0xFFFF) {
            serialNo = 0;
        }
        return serialNo++;
    }

    public String format() {
        return "ackType=" + getAckType() + ",cmdType=" + getCmdType() + ",serialNo=" + getSerialNo()
                + ",passType=" + getPassType() + ",passRoute=" + getPassRoute() + ",passContent=[" + passCommand.format() + "]";
    }

}
