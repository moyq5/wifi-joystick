package com.moyq5.wifi.joystick;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class BootActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boot);
        Button serverBtn = findViewById(R.id.server);
        serverBtn.setOnClickListener(v -> startActivity(new Intent(getApplication(), MainActivity.class)));
        Button clientBtn = findViewById(R.id.client);
        clientBtn.setOnClickListener(v -> startActivity(new Intent(getApplication(), ClientActivity.class)));
    }
}